from django.conf.urls import url, include
from GestionAcademica import views

urlpatterns=[
    url(r'^$', views.index, name="index"),
    url(r'^crearAlumno$', views.crearAlumno, name="crearAlumno"),
]